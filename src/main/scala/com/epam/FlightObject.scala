package com.epam

/**
  * @author Dmitrii_Kniazev 
  * @since 01/31/2017
  */
object FlightObject {
  private val fieldNames = Seq(
    "year",
    "month",
    "dayofmonth",
    "dayofweek",
    "deptime",
    "crsdeptime",
    "arrtime",
    "crsarrtime",
    "uniquecarrier",
    "flightnum",
    "tailnum",
    "actualelapsedtime",
    "crselapsedtime",
    "airtime",
    "arrdelay",
    "depdelay",
    "origin",
    "dest",
    "distance",
    "taxiin",
    "taxiout",
    "cancelled",
    "cancellationcode",
    "diverted",
    "carrierdelay",
    "weatherdelay",
    "nasdelay",
    "securitydelay",
    "lateaircraftdelay"
  )

  //2007,1,1,1,1232,1225,1341,1340,WN,2891,N351,69,75,54,1,7,SMF,ONT,389,4,11,0,,0,0,0,0,0,0
  def parseString(str: String): Map[String, Any] = {
    var resultMap = Map[String, Any]()
    val split = str.split(",").iterator
    val fields = fieldNames.iterator
    while (split.hasNext && fields.hasNext) {
      resultMap += (fields.next() -> split.next())
    }
    resultMap
  }
}
