package com.epam

import java.io.BufferedReader

import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.receiver.Receiver

/**
  * @author Dmitrii_Kniazev 
  * @since 01/31/2017
  */
class LocalFileReceiver(val path: String)
  extends Receiver[String](StorageLevel.MEMORY_ONLY_2) {
  override def onStart(): Unit = {
    // Start the thread that receives data over a connection
    new Thread("Socket Receiver") {
      override def run() {
        receive()
      }
    }.start()
  }

  override def onStop(): Unit = {
    // There is nothing much to do as the thread calling receive()
    // is designed to stop by itself if isStopped() returns false
  }

  private def receive() = {
    var userInput: String = null
    try {
      val reader = new BufferedReader(scala.io.Source.fromFile(path).reader())
      userInput = reader.readLine()
      while (!isStopped && userInput != null) {
        store(userInput)
        userInput = reader.readLine()
      }
      reader.close()
    } catch {
      case t: Throwable =>
        // restart if there is any other error
        restart("Error receiving data", t)
    }
  }

}
