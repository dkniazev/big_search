package com.epam

import org.apache.log4j.Logger
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}
import org.elasticsearch.spark.streaming._

/**
  * @author Dmitrii_Kniazev 
  * @since 12/07/2016
  */
object Spark2ElasticApp extends App {
  private val LOG: Logger = Logger.getLogger(Spark2ElasticApp.getClass)

  if (args.length < 1) {
    LOG.error("Current number of parameters" + args.length)
    LOG.error("Usage: spark2elastic <in>")
    System.exit(2)
  }
  private val inPath = args(0)

  private val appName = "Spark2Elastic"
  private val masterUrl = "local"
  private val conf = new SparkConf().setAppName(appName)
  //set local mode
  if (args.length == 2 && args(1) == masterUrl) {
    conf.setMaster(masterUrl + "[2]")
  }
  conf.set("es.index.auto.create", "true")
  conf.set("es.nodes.wan.only", "true")
  conf.set("es.nodes", "192.168.99.100")
  conf.set("es.port", "9200")

  private val sc = new SparkContext(conf)
  private val ssc = new StreamingContext(sc, Seconds(1))

  private val textStream = ssc.receiverStream(new LocalFileReceiver(inPath))

  private val result = textStream.map(FlightObject.parseString)
  result.count().print()

  result.saveToEs("flights/{year}")

  ssc.start()
  ssc.awaitTermination()
}
