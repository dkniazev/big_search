package com.epam


import com.holdenkarau.spark.testing.SharedSparkContext
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
  * @author Dmitrii_Kniazev 
  * @since 12/07/2016
  */
@RunWith(classOf[JUnitRunner])
class Spark2ElasticAppTest extends FunSuite with SharedSparkContext {

  test("Str2MapTransformation") {
    val source = Array(
      "2007,1,1,1,1232,1225,1341,1340,WN,2891,N351,69,75,54,1,7,SMF,ONT,389,4,11,0,,0,0,0,0,0,0",
      "2007,1,1,1,2002,1935,2115,2055,WN,1784,N711HK,73,80,61,20,27,SMF,SNA,404,2,10,0,,0,19,0,0,0,1"
    )
    val expected = Array(Map(
      "year" -> "2007",
      "month" -> "1",
      "dayofmonth" -> "1",
      "dayofweek" -> "1",
      "deptime" -> "1232",
      "crsdeptime" -> "1225",
      "arrtime" -> "1341",
      "crsarrtime" -> "1340",
      "uniquecarrier" -> "WN",
      "flightnum" -> "2891",
      "tailnum" -> "N351",
      "actualelapsedtime" -> "69",
      "crselapsedtime" -> "75",
      "airtime" -> "54",
      "arrdelay" -> "1",
      "depdelay" -> "7",
      "origin" -> "SMF",
      "dest" -> "ONT",
      "distance" -> "389",
      "taxiin" -> "4",
      "taxiout" -> "11",
      "cancelled" -> "0",
      "cancellationcode" -> "",
      "diverted" -> "0",
      "carrierdelay" -> "0",
      "weatherdelay" -> "0",
      "nasdelay" -> "0",
      "securitydelay" -> "0",
      "lateaircraftdelay" -> "0"
    ), Map(
      "year" -> "2007",
      "month" -> "1",
      "dayofmonth" -> "1",
      "dayofweek" -> "1",
      "deptime" -> "2002",
      "crsdeptime" -> "1935",
      "arrtime" -> "2115",
      "crsarrtime" -> "2055",
      "uniquecarrier" -> "WN",
      "flightnum" -> "1784",
      "tailnum" -> "N711HK",
      "actualelapsedtime" -> "73",
      "crselapsedtime" -> "80",
      "airtime" -> "61",
      "arrdelay" -> "20",
      "depdelay" -> "27",
      "origin" -> "SMF",
      "dest" -> "SNA",
      "distance" -> "404",
      "taxiin" -> "2",
      "taxiout" -> "10",
      "cancelled" -> "0",
      "cancellationcode" -> "",
      "diverted" -> "0",
      "carrierdelay" -> "19",
      "weatherdelay" -> "0",
      "nasdelay" -> "0",
      "securitydelay" -> "0",
      "lateaircraftdelay" -> "1"
    ))

    val input = sc.parallelize(source)
    val result = input.map(FlightObject.parseString).collect()
    assert(result === expected)
  }
}
